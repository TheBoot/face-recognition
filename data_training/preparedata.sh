#!/bin/sh

a=1

while [ $a -lt 34 ]
    do
        echo $a
        if [ $a -eq 34 ]
        then
        break
        fi
        mkdir "S$a"
        ../src/preparationFace \
            --cascade="/usr/local/share/OpenCV/haarcascades/haarcascade_frontalface_alt.xml" \
            --nested-cascade="/usr/local/share/OpenCV/haarcascades/haarcascade_eye_tree_eyeglasses.xml" \
            --scale=2 \
            /home/alhakam/Data/doc-face/faces/"s$a"/ \
            /home/alhakam/Data/workspace/FaceRecognition/data_training/"S$a"/
         a=`expr $a + 1`
    done
