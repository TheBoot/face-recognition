/*
 * =====================================================================================
 *
 *       Filename:  FaceRecognize.hpp
 *
 *    Description:   - how to train
 *                       1. trainPreparation()
 *                          return 0 if successed
 *                       2. createCSV()
 *                          return 0 if successed
 *                       3. fisherfaceTrain()
 *                          return 0 if successed
 *                       4. getlabel()
 *                          please save label return on apps
 *                   - how to recognize face
 *                       1. faceRecognition()
 *                          - return label if succes recognize
 *                          - compare return label from faceRecognition with
 *                            return label from getlabel
 *
 *        Version:  1.0
 *        Created:  12/09/17 11:05:37
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Izzul Haq Alhakam (al), izzul@bahasakita.co.id
 *   Organization:  PT. Bahasa Kinerja Utama
 *
 * =====================================================================================
 */

#include <opencv2/core.hpp>
#include <opencv2/face.hpp>
#include <opencv2/face/facerec.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/objdetect.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/imgproc.hpp>
#include <iostream>
#include <stdio.h>
#include <string>
#include <sstream>

using namespace cv;

using std::cout;
using std::cerr;
using std::endl;
using std::string;
using std::vector;
using std::ifstream;
using std::stringstream;

// Read Image on File CSV
static void dbread( const string &filename, vector<Mat> &images, vector<int> &labels,
                    char separator = ';') {

    ifstream file(filename.c_str(), ifstream::in);

    if (!file) {
        string error =  "no valid input file";
        CV_Error(CV_StsBadArg, error);
    }

    string line, path, label;
    while (getline(file, line)) {
        stringstream liness(line);
        getline(liness, path, separator);
        getline(liness, label);
        if (!path.empty() && !label.empty()) {
            images.push_back(imread(path, 0));
            labels.push_back(atoi(label.c_str()));
        }
    }
}

// Create Base Path Image
void readFilenames(vector<string> &filenames, const string &directory) {

    DIR *dir;
    class dirent *ent;
    class stat st;

    dir = opendir(directory.c_str());
    while ((ent = readdir(dir)) != NULL) {
        const string file_name = ent->d_name;
        const string full_file_name = directory + "/" + file_name;

        if (file_name[0] == '.')
           continue;

        if (stat(full_file_name.c_str(), &st) == -1)
            continue;

        const bool is_directory = (st.st_mode & S_IFDIR) != 0;

        if (is_directory)
            continue;

        filenames.push_back(file_name);
        std::cout<<file_name<<std::endl;
    }
    closedir(dir);
}

// Detection Face and write face on Preparation Image
void detectFace( Mat &img, CascadeClassifier& cascade,
             CascadeClassifier& nestedCascade,
             double scale, bool tryflip, const string &dest ) {

    double t = 0;
    vector<Rect> faces, faces2;

    const static Scalar colors[] = {
        Scalar(255,0,0),
        Scalar(255,128,0),
        Scalar(255,255,0),
        Scalar(0,255,255),
        Scalar(0,128,255),
        Scalar(0,255,255),
        Scalar(0,0,255),
        Scalar(255,0,255)
    };

    Mat gray, smallImg;

    cvtColor(img, gray, COLOR_BGR2GRAY);
    //Size size(100,100);
    double fx = 1 / scale;
    resize( gray, smallImg, Size(), fx, fx, INTER_LINEAR );
    //resize(gray, smallImg, size);

    t = (double)getTickCount();
    cascade.detectMultiScale(smallImg, faces, 1.1, 2, 0 | CASCADE_SCALE_IMAGE, Size(30,30));
    if ( tryflip ) {
        flip (smallImg, smallImg, 1);
        cascade.detectMultiScale(smallImg, faces2, 1.1, 2, 0 | CASCADE_SCALE_IMAGE, Size(30,30));
        for( vector<Rect>::const_iterator r = faces.begin(); r != faces2.end(); ++r) {
            faces.push_back(Rect(smallImg.cols - r->x -r->width,
                            r->y, r->width, r->height));
        }
    }
    t = (double)getTickCount() - t;
    printf("detection time = %g  ms\n", t*1000/getTickFrequency());

    for ( size_t i = 0; i < faces.size(); i++) {
        Rect r = faces[i];
        Mat smallImgROI;
        vector<Rect> nestedObjects;
        Point center;
        Scalar color = colors[i%8];
        int radius;

        double aspect_ratio = (double)r.width/r.height;

        if ( 0.75 < aspect_ratio && aspect_ratio < 1.3) {
            center.x = cvRound((r.x + r.width*0.5)*scale);
            center.y = cvRound((r.y + r.height*0.5)*scale);
            radius = cvRound((r.width + r.height)*0.25*scale);
            circle( img, center, radius, color, 3, 8, 0);
        }
        else {
            rectangle( img, cvPoint(cvRound(r.x*scale), cvRound(r.y*scale)),
                       cvPoint(cvRound((r.x + r.width-1)*scale), cvRound((r.y + r.height-1)*scale)),
                       color, 3, 8, 0);
    }
        if (nestedCascade.empty())
            continue;

        smallImgROI = smallImg( r );
        nestedCascade.detectMultiScale (smallImgROI, nestedObjects, 1.1, 2, 0 |CASCADE_SCALE_IMAGE,
                                         Size(30,300));
        // Save Image
        resize (smallImgROI, smallImgROI, Size(100,100));
        imwrite(dest, smallImgROI);
    }

}

// Write Preparation Data on destination directory
// Call in main Program
int trainPreparation(string &cascadeName,
                     string &nestedCascadeName,
                     string &sourcedir, string &destdir,
                     double scale, bool tryflip) {

    vector<string> filenames;
    CascadeClassifier cascade, nestedCascade;

    if (scale < 1)
        scale = 1;

    if (!nestedCascade.load(nestedCascadeName))
        std::cerr << "WARNING: could not load classifier cascade for nested objects" << std::endl;
    if (!cascade.load(cascadeName)) {
        std::cerr << "Error: Could not load classifier cascade" << std::endl;
        return -1;
    }
    if (sourcedir.empty()||destdir.empty()) {
        std::cerr << "Error: Could not find image" << std::endl;
    }
    else {

        readFilenames(filenames, sourcedir);

       for (size_t i = 0; i < filenames.size(); ++i) {

        Mat image=imread(sourcedir + filenames[i], IMREAD_COLOR);

            if(!image.data) {
                std::cerr<<"problem loading image!"<< std::endl;
            }

        std::cout<<folder+filenames[i]<<std::endl;


        // Detecting Face and Write to destination directory
        detect(image, cascade, nestedCascade,
               scale, tryflip, destdir+filenames[i]);
       }
    return 0;
}


// Create CSV file (dependency of fisherface train or eigenface train)
int createCSV (const string &base_path, const string &output) {
    std::stringstream ss;
    ss << "./create_csv.py ";
    ss << base_path << " >";
    ss << output;

   // Call Python Binary
   // "./create_scv ../data/src_img/ > ../date/out_img/"
   system(ss.str());
   return 0;
}

// Get Label by path image
string getlabel(const string &filename, const string &dir_image, char separator = ';') {

    ifstream file(filename.c_str(), ifstream::in);

    if (!file) {
        string error =  "no valid input file";
        CV_Error(CV_StsBadArg, error);
    }

    string line, path, label;
    while (getline(file, line)) {
        stringstream liness(line);
        getline(liness, path, separator);
        getline(liness, label);
        if (path.compare(dir_image)==0) {
            return label;
        }
    }
}

// Train with Fisherface algorithm
int fisherfaceTrain (const string &csvfile) {

    // FisherFace Trainer
    vector<Mat> images;
    vector<int> labels;

    try {
        dbread(csvfile, images, labels);

        cout << "size of the images is " << images.size() << endl;
        cout << "size of the labels is " << images.size() << endl;
        cout << "Training begins ..... " << images.size() << endl;
    }
    catch (cv::Exception &e) {
        cerr << "Error opening the file " << e.msg << endl;
        exit(1);
    }

    // create algorithm fisherface recognizer
    cv::Ptr<cv::face::FaceRecognizer> model = cv::face::FisherFaceRecognizer::create();

    model->train(images, labels);

    int height = images[0].rows;

    // Save Model fisherface
    model->write("/home/alhakam/Data/workspace/FaceRecognition/fisherface.yml");

    cout << "Training Finished........" << endl;

    return 0;
}

int faceRecognition(string &classifier, string &modelface, string &imagefile, 
                    double confidence_level) {

    int i;
    int label = -1;
    double confidence = 0;
    vector<Rect> faces;

    CascadeClassifier face_cascade;
    Mat OrigFace;
    Mat GrayscaleFace;


    // Using Fisherface recognition
    cv::Ptr<cv::face::FaceRecognizer> model = cv::face::FisherFaceRecognizer::create(0, confidence_level);

    // Read fisherface algorithm
    model->read(modelface);

    // Read File Classifier
    if (!face_cascade.load(classifier)) {
        cout << "Error loading file" << endl;
        return -1;
    }

    // Get Image
    Mat OrigFace = imread(imagefile);

    /* Normalize Image */
    // Convert Image to grayscale
    cvtColor(OrigFace, GrayscaleFace, CV_BGR2GRAY);

    // Detect Image in gray image
    face_cascade.detectMultiScale(grayScaleFace, faces, 1.1, 5, 0, cv::Size(100,100));

    // Number Face Detected
    cout << faces.size() << " Faces Detected" << endl;
    for (i=0; i<faces.size(); i++) {

        // Region Of Interest
        Rect face_i = faces[i];

        // Crop the ROI from Gray Image
        Mat face = grayScaleFace(face_i);

        // Resizing the cropped image to suit
        // database image sizes
        Mat face_resized;
        resize(face, face_resized, Size(100, 100), 1.0, 1.0, INTER_CUBIC);

        // Recognizing what faces detected
        model->predict(face_resized, label, confidence);

        cout << " Threshold : " << model->getThreshold() << endl;
        cout << " confidence : " << confidence << endl;
        cout << " label : " << label << endl;
    }

    // return value of label
    return label;
}
