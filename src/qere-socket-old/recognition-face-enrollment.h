/*
 * =====================================================================================
 *
 *       Filename:  recognition-face-enrollment.h
 *
 *    Description
 *
 *        Version:  1.0
 *        Created:  13/09/17 14:09:20
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Izzul Haq Alhakam (al), izzul@bahasakita.co.id
 *   Organization:  PT. Bahasa Kinerja Utama
 *
 * =====================================================================================
 */

int enrollment_face(struct lws *wsi, struct face_recognition_payment_data_monitoring_agent *frc, json_object *jo_req);
