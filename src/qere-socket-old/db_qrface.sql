--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.4
-- Dumped by pg_dump version 9.6.4

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: enrolled_faces; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE enrolled_faces (
    id integer NOT NULL,
    id_user integer,
    path character varying,
    created_by character varying(128),
    created_time timestamp without time zone DEFAULT now()
);


ALTER TABLE enrolled_faces OWNER TO root;

--
-- Name: enrolled_faces_id_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE enrolled_faces_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE enrolled_faces_id_seq OWNER TO root;

--
-- Name: enrolled_faces_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: root
--

ALTER SEQUENCE enrolled_faces_id_seq OWNED BY enrolled_faces.id;


--
-- Name: qrcodes; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE qrcodes (
    id integer NOT NULL,
    seller character varying(128),
    created_time timestamp without time zone DEFAULT now(),
    sell_value numeric,
    created_by character varying(128),
    uuid character varying(37)
);


ALTER TABLE qrcodes OWNER TO root;

--
-- Name: qrcodes_id_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE qrcodes_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE qrcodes_id_seq OWNER TO root;

--
-- Name: qrcodes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: root
--

ALTER SEQUENCE qrcodes_id_seq OWNED BY qrcodes.id;


--
-- Name: seller_details; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE seller_details (
    id integer NOT NULL,
    id_user integer,
    company_code character varying(3),
    account_no character varying(128),
    created_by character varying(128),
    created_time timestamp without time zone DEFAULT now()
);


ALTER TABLE seller_details OWNER TO root;

--
-- Name: seller_details_id_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE seller_details_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE seller_details_id_seq OWNER TO root;

--
-- Name: seller_details_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: root
--

ALTER SEQUENCE seller_details_id_seq OWNED BY seller_details.id;


--
-- Name: users; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE users (
    id integer NOT NULL,
    name character varying(128),
    password character varying,
    type character varying(10)
);


ALTER TABLE users OWNER TO root;

--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE users_id_seq OWNER TO root;

--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: root
--

ALTER SEQUENCE users_id_seq OWNED BY users.id;


--
-- Name: validated; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE validated (
    id integer NOT NULL,
    qrcodes_id integer,
    buyer character varying(128),
    qr boolean,
    face boolean
);


ALTER TABLE validated OWNER TO root;

--
-- Name: validated_id_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE validated_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE validated_id_seq OWNER TO root;

--
-- Name: validated_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: root
--

ALTER SEQUENCE validated_id_seq OWNED BY validated.id;


--
-- Name: enrolled_faces id; Type: DEFAULT; Schema: public; Owner: root
--

ALTER TABLE ONLY enrolled_faces ALTER COLUMN id SET DEFAULT nextval('enrolled_faces_id_seq'::regclass);


--
-- Name: qrcodes id; Type: DEFAULT; Schema: public; Owner: root
--

ALTER TABLE ONLY qrcodes ALTER COLUMN id SET DEFAULT nextval('qrcodes_id_seq'::regclass);


--
-- Name: seller_details id; Type: DEFAULT; Schema: public; Owner: root
--

ALTER TABLE ONLY seller_details ALTER COLUMN id SET DEFAULT nextval('seller_details_id_seq'::regclass);


--
-- Name: users id; Type: DEFAULT; Schema: public; Owner: root
--

ALTER TABLE ONLY users ALTER COLUMN id SET DEFAULT nextval('users_id_seq'::regclass);


--
-- Name: validated id; Type: DEFAULT; Schema: public; Owner: root
--

ALTER TABLE ONLY validated ALTER COLUMN id SET DEFAULT nextval('validated_id_seq'::regclass);


--
-- Data for Name: enrolled_faces; Type: TABLE DATA; Schema: public; Owner: root
--

COPY enrolled_faces (id, id_user, path, created_by, created_time) FROM stdin;
\.


--
-- Name: enrolled_faces_id_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('enrolled_faces_id_seq', 1, false);


--
-- Data for Name: qrcodes; Type: TABLE DATA; Schema: public; Owner: root
--

COPY qrcodes (id, seller, created_time, sell_value, created_by, uuid) FROM stdin;
1	cashier01	2017-09-12 14:53:19.526057	50000	50000	714fa8a4-978f-11e7-98a6-080027b7e475
2	cashier01	2017-09-12 14:56:49.386953	50000	cashier01	ee65ef38-978f-11e7-98a6-080027b7e475
3	cashier01	2017-09-12 15:03:01.313424	50000	cashier01	cc1574b6-9790-11e7-98a6-080027b7e475
4	cashier01	2017-09-12 15:04:01.921517	50000	cashier01	f03584d0-9790-11e7-98a6-080027b7e475
5	cashier01	2017-09-12 15:05:56.191812	50000	cashier01	3451bc10-9791-11e7-98a6-080027b7e475
6	cashier01	2017-09-12 15:06:54.270215	50000	cashier01	56efd806-9791-11e7-98a6-080027b7e475
7	cashier01	2017-09-12 15:08:39.484604	50000	cashier01	95a64972-9791-11e7-98a6-080027b7e475
8	cashier01	2017-09-12 15:43:22.127468	50000	cashier01	6f004ebc-9796-11e7-98a6-080027b7e475
9	cashier01	2017-09-12 15:53:10.63539	50000	cashier01	cdc78220-9797-11e7-98a6-080027b7e475
10	2#100	2017-09-12 17:08:41.313087	50000	2#100	983#100101170912#100
11	cashier01	2017-09-12 17:12:39.231392	50000	cashier01	0101170912
12	cashier01	2017-09-12 17:14:13.905471	50000	cashier01	0101170912
13	cashier01	2017-09-12 17:17:16.937676	50000	cashier01	101170912
14	cashier01	2017-09-12 17:20:08.619668	50000	cashier01	101170912
15	cashier01	2017-09-12 17:20:52.427133	50000	cashier01	101170912
16	810	2017-09-12 17:21:38.878108	50000	810	683810101170912810
17	f10	2017-09-12 17:26:22.408775	50000	f10	983f10101170912f10
18	k10	2017-09-12 17:28:36.702251	50000	k10	291k10101170912k10
19	100	2017-09-12 17:30:18.89667	50000	100	402100101170912100
20		2017-09-13 08:13:22.761784	50000		208101170913100
21		2017-09-13 08:31:22.010287	50000		446101170913100
22		2017-09-13 08:31:44.800067	50000		301101170913100
23		2017-09-13 08:31:44.927105	50000		301101170913100
24		2017-09-13 08:33:40.8357	50000		167101170913100
25		2017-09-13 08:34:59.182652	50000		668101170913100
26		2017-09-13 09:13:33.907309	50000		869101170913100
27		2017-09-13 09:42:51.467193	50000		348101170913100
\.


--
-- Name: qrcodes_id_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('qrcodes_id_seq', 27, true);


--
-- Data for Name: seller_details; Type: TABLE DATA; Schema: public; Owner: root
--

COPY seller_details (id, id_user, company_code, account_no, created_by, created_time) FROM stdin;
1	1	101	100010001000	admin	2017-09-12 16:06:36.351945
\.


--
-- Name: seller_details_id_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('seller_details_id_seq', 1, true);


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: root
--

COPY users (id, name, password, type) FROM stdin;
1	cashier01	5e0fb950a170f26f6a071be306cb48ab	seller
\.


--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('users_id_seq', 1, true);


--
-- Data for Name: validated; Type: TABLE DATA; Schema: public; Owner: root
--

COPY validated (id, qrcodes_id, buyer, qr, face) FROM stdin;
\.


--
-- Name: validated_id_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('validated_id_seq', 1, false);


--
-- PostgreSQL database dump complete
--

