/**
 * Filename: /home/raha/qere-socket/recognition-client-function.c
 * Path: /home/raha/qere-socket
 * Created Date: Monday, September 11th 2017, 3:53:19 pm
 * Author: raha
 * 
 * Copyright (c) 2017 PT Bahasa Kinerja Utama
 */

 #include "recognition.h"

 #define BUFFER_LEN     1024 * 1000
 #define DB_CONN_STRING "user=root password=555913 dbname=qrface"
 #define FOLDER_PHOTO   "/app/frc/photos/"

 char * getDateYYMMDD() {
     time_t rawtime = time (NULL);
     struct tm *tm = localtime(&rawtime);
     char * date_string = (char *)malloc(8 * sizeof(char));
     //int    n;

     strftime(date_string, sizeof(date_string), "%y%m%d", tm);
     //date_string[n] ='\0';

     return date_string;
 }

 int open_db_connection(struct face_recognition_payment_data_monitoring_agent *frc){
    frc->conn = PQconnectdb(DB_CONN_STRING);
    if (PQstatus(frc->conn) == CONNECTION_BAD) {
        fprintf(stderr, "Connection to database failed: %s\n",
            PQerrorMessage(frc->conn));
    
            PQfinish(frc->conn);
            return -1;
    }
    lwsl_notice("Connection Opened.\n");
    return 0;
 }

 void write_to_browser(struct lws *wsi, struct face_recognition_payment_data_monitoring_agent *frc, char *label, json_object *value) {
     char out_brw[1024 * 100];
     json_object *jo_res;
      
     jo_res = json_object_new_object();
     json_object_object_add(jo_res, "label", json_object_new_string(label));
     json_object_object_add(jo_res, "value", value);

     lwsl_notice("write_to_browser_ lenth: %d", sizeof(json_object_to_json_string(jo_res)));
     int n = sprintf(out_brw, "%s", json_object_to_json_string(jo_res));

     out_brw[n] = '\0';
     enq(frc->q, out_brw);
     lwsl_notice(out_brw);

     json_object_put(jo_res);
     lwsl_notice("jo_res released!");
     lws_callback_on_writable(frc->wsi_ori);
     lwsl_notice("lws_callback_on_writeable executed!");
 }

 int login_response(struct lws *wsi,
    struct face_recognition_payment_data_monitoring_agent *frc, json_object *jo_req){

    PGresult    *res;
    char        *stm;
    char        *paramValues[3];
    char        p_value1[256];
    char        p_value2[256];
    char        p_value3[256];
    int         n;

    json_object *obj_value;

    obj_value   = json_object_object_get(jo_req, "user_id");
    if(obj_value == NULL) return(-1);
    strcpy(p_value1, json_object_get_string(obj_value));

    obj_value   = json_object_object_get(jo_req, "password");
    if(obj_value == NULL) return(-1);
    strcpy(p_value2, json_object_get_string(obj_value));

    obj_value   = json_object_object_get(jo_req, "type");
    if(obj_value == NULL) return(-1);
    strcpy(p_value3, json_object_get_string(obj_value));

    paramValues[0] = &p_value1[0];
    paramValues[1] = &p_value3[0];
    paramValues[2] = &p_value2[0];

    //create statement & execute it
    stm = (char *)"SELECT user_id, type FROM users WHERE user_id = $1 AND password = md5($2) AND type = $3";
    
    res = PQexecParams(frc->conn, stm, 3, NULL, 
            (const char * const*) paramValues, NULL, NULL, 0);
    
    if (PQresultStatus(res) == PGRES_TUPLES_OK) {
        n = PQntuples(res);

        if (n == 1) {
            obj_value = json_object_new_object();
            json_object_object_add(obj_value, "code",
                json_object_new_string("00"));
            json_object_object_add(obj_value, "user_id",
                json_object_new_string(PQgetvalue(res, 0, 0)));
            json_object_object_add(obj_value, "type",
                json_object_new_string(PQgetvalue(res, 0, 1)));
            
            write_to_browser(wsi, frc, (char *)"login-response", obj_value);

            strcpy(frc->user_id, p_value1);
            strcpy(frc->type, p_value3);
        } else {
            obj_value = json_object_new_object();
            json_object_object_add(obj_value, "code",
                json_object_new_string("99"));
            write_to_browser(wsi, frc, (char *)"login-response", obj_value);
      
        }
    }

    PQclear(res);
    return 0;
 }

    int request_qr(struct lws *wsi, struct face_recognition_payment_data_monitoring_agent *frc, json_object *jo_req) {
            
        PGresult    *res;
        char        *stm;
        char        *paramValues[4];
        char        p_value1[256];
        char        p_value2[256];
        char        qrCodeUniqueId[15];
        time_t      t;
        

        //uuid_t      uuid;
        //char        uuid_str[37];

        json_object *obj_value;
        
        srand((unsigned) time(&t));

        obj_value   = json_object_object_get(jo_req, "seller");
        if(obj_value == NULL) return(-1);
        strcpy(p_value1, json_object_get_string(obj_value));
    
        obj_value   = json_object_object_get(jo_req, "sell_value");
        if(obj_value == NULL) return(-1);
        strcpy(p_value2, json_object_get_string(obj_value));

        /*uuid_generate_time(uuid);
        sprintf(uuid_str, "%02x%02x%02x%02x-%02x%02x-%02x%02x-%02x%02x-%02x%02x%02x%02x%02x%02x", 
        uuid[0], uuid[1], uuid[2], uuid[3], uuid[4], uuid[5], uuid[6], uuid[7],
        uuid[8], uuid[9], uuid[10], uuid[11], uuid[12], uuid[13], uuid[14], uuid[15]
        );
        lwsl_notice("uuid_str: %s", uuid_str);*/

        //create qrcodes "uuid"
        paramValues[0] = p_value1;

        stm = (char *)"SELECT b.company_code, b.account_no FROM users a INNER JOIN seller_details b ON a.id = b.id_user WHERE a.name = $1 AND a.type = 'seller'";

        res = PQexecParams(frc->conn, stm, 1, NULL,
            (const char * const*) paramValues, NULL, NULL, 0);

        if (PQresultStatus(res) == PGRES_TUPLES_OK) {

           // qrCodeUniqueId[0] = '\0';
            memset(qrCodeUniqueId, 0, sizeof qrCodeUniqueId);
            qrCodeUniqueId[0] ='\0'; 
            
            int threeDigitRand = rand() % 900 + 100;
            lwsl_notice("threeDigitRand: %d", threeDigitRand);
            char strTDR[3];
            sprintf(strTDR, "%d", threeDigitRand); 
            lwsl_notice("strTDR: %s", strTDR);

            strcat(qrCodeUniqueId, strTDR);
            memset(strTDR, 0, sizeof strTDR);
            
            char companyCode[3];
            strcpy(companyCode, PQgetvalue(res, 0, 0));
            //char *companyCode = PQgetvalue(res, 0, 0);
            lwsl_notice("company code: %s", companyCode);
            
            strcat(qrCodeUniqueId, companyCode);
            memset(companyCode, 0, sizeof companyCode);

            char * date_string;
            date_string = getDateYYMMDD();
            lwsl_notice("date string: %s", date_string);

            strcat(qrCodeUniqueId, date_string);

            char account_str[13];
            strcpy(account_str, PQgetvalue(res, 0, 1));
            lwsl_notice("account_str: %s", account_str);
            int pos = 0;
            int length = 3;
            int ctr = 0;
            char account_str_sub[3];

            while (ctr < length) {
                account_str_sub[ctr] = account_str[pos+ctr];
                ctr++;
            }
            account_str_sub[ctr] = '\0';
            lwsl_notice("account_str_sub: %s", account_str_sub);

            strcat(qrCodeUniqueId, account_str_sub);
            memset(account_str_sub, 0, sizeof account_str_sub);

            lwsl_notice ("Qr codes id : %s", qrCodeUniqueId);
        } else {
            lwsl_notice("error");
            lwsl_notice(PQerrorMessage(frc->conn));
            return 0;
        }

        paramValues[0] = p_value1;
        paramValues[1] = p_value2;
        paramValues[2] = qrCodeUniqueId;
        paramValues[3] = p_value1;

        stm = (char *)"INSERT INTO qrcodes (seller, sell_value, uuid, created_by) VALUES ($1, $2, $3, $4)";
   
        res = PQexecParams(frc->conn, stm, 4, NULL, 
            (const char * const*) paramValues, NULL, NULL, 0);

            
        if (PQresultStatus(res) == PGRES_COMMAND_OK) {
            obj_value = json_object_new_object();
            json_object_object_add(obj_value, "code",
                json_object_new_string("00"));
            json_object_object_add(obj_value, "uuid",
                json_object_new_string(qrCodeUniqueId));
            json_object_object_add(obj_value, "seller",
                json_object_new_string(p_value1));
            json_object_object_add(obj_value, "sell_value",
                json_object_new_string(p_value2));
            
            write_to_browser(wsi, frc, (char *)"request-qr-response", obj_value);
        } else {
                lwsl_notice("Error: %s", PQerrorMessage(frc->conn));
                obj_value = json_object_new_object();
                json_object_object_add(obj_value, "code",
                    json_object_new_string("99"));
                write_to_browser(wsi, frc, (char *)"request-qr-response", obj_value);
        }

        PQclear(res);
        return 0;
    }

    int validate_qr(struct lws *wsi, struct face_recognition_payment_data_monitoring_agent *frc, json_object *jo_req) {
            
        PGresult    *res;
        char        *stm;
        char        *paramValues[4];
        char        p_value1[256];
        char        p_value2[256];
        char        p_value3[256];    
        //int         n;
        char        *qrid;

        //uuid_t      unique_id;
        //char        uuid_str[37];

        json_object *obj_value;

        obj_value   = json_object_object_get(jo_req, "uuid");
        if(obj_value == NULL) return(-1);
        strcpy(p_value1, json_object_get_string(obj_value));

        obj_value   = json_object_object_get(jo_req, "buyer");
        strcpy(p_value2, json_object_get_string(obj_value));

        paramValues[0] = p_value1;
        
        stm = (char *)"SELECT id, uuid FROM qrcodes WHERE uuid = $1";

        res = PQexecParams(frc->conn, stm, 1, NULL, (const char * const*) paramValues, NULL, NULL, 0);

        if (PQresultStatus(res) == PGRES_TUPLES_OK) {
            qrid = PQgetvalue(res, 0, 0);

            paramValues[0] = qrid;
            paramValues[1] = p_value2;

            stm = (char *)"INSERT INTO validated(qrcodes_id, buyer, qr, face) VALUES($1, $2, true, false)";
            
            res = PQexecParams(frc->conn, stm, 2, NULL, (const char * const*) paramValues, NULL, NULL, 0);

            if (PQresultStatus(res) == PGRES_COMMAND_OK) {
                obj_value = json_object_new_object();
                json_object_object_add(obj_value, "code",
                    json_object_new_string("00"));
                json_object_object_add(obj_value, "uuid",
                    json_object_new_string(p_value1));
                json_object_object_add(obj_value, "seller",
                    json_object_new_string(p_value2));
                json_object_object_add(obj_value, "buyer",
                    json_object_new_string(p_value3));
                
                write_to_browser(wsi, frc, (char *)"validate-qr-response", obj_value);
            } else {
                    obj_value = json_object_new_object();
                    json_object_object_add(obj_value, "code",
                        json_object_new_string("99"));
                    write_to_browser(wsi, frc, (char *)"validate-qr-response", obj_value);
            }
        }

        PQclear(res);
        return 0;
    }

    int validate_face (struct lws *wsi,struct face_recognition_payment_data_monitoring_agent *frc, json_object *jo_req) {
      
      return 0;  
    }








