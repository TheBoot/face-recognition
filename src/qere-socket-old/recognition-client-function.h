int open_db_connection(struct face_recognition_payment_data_monitoring_agent *frc);
int login_response(struct lws *wsi,
    struct face_recognition_payment_data_monitoring_agent *frc, json_object *jo_req);
int request_qr(struct lws *wsi, face_recognition_payment_data_monitoring_agent *frc, json_object *jo_req);
int validate_qr(struct lws *wsi, face_recognition_payment_data_monitoring_agent *frc, json_object *jo_req);