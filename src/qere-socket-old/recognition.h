/**
 * Filename: /home/raha/qere-socket/recognition.h
 * Path: /home/raha/qere-socket
 * Created Date: Monday, September 11th 2017, 11:42:17 am
 * Author: raha
 * 
 * Copyright (c) 2017 PT Bahasa Kinerja Utama
 */


#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <signal.h>
#include <string.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <assert.h>
#include <uuid/uuid.h>

#include "libwebsockets.h"
#include "lws_config.h"

#ifdef _WIN32
#include <io.h>
#include "gettimeofday.h"
#else 
#include <syslog.h>
#include <sys/time.h>
#include <unistd.h>
#endif

#include <postgresql/libpq-fe.h>
#include <json-c/json.h>
#include "recognition-queue.h"

extern int close_testing;

extern volatile int force_exit;
extern struct lws_context *context;

extern void nttm_server_lock(int care);
extern void nttm_server_unlock(int care);

#ifndef __func__
#define __funct__ __FUNCTION__
#endif

struct face_recognition_payment_data_monitoring_agent{
    char identity[12];
    char in_brw[1024];
    int  cnt;
    int  fd;

    // db connection
    PGconn  *conn;

    // message queue
    struct nqueue *q;

    // user 
    char user_id[32];
    char type[10];

    struct lws *wsi_ori;
};

extern int callback_monitoring_agent(struct lws * wsi, enum lws_callback_reasons reason, void *user, void *in , size_t len);

char * getDateYYMMDD();