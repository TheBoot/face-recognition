/*
 * =====================================================================================
 *
 *       Filename:  recognition-face-verification.cc
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  13/09/17 14:32:47
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Izzul Haq Alhakam (al), izzul@bahasakita.co.id
 *   Organization:  PT. Bahasa Kinerja Utama
 *
 * =====================================================================================
 */

#include "recognition.h"
#include "recognition-client-function.h"
#include "FaceRecognize.hpp"

int validate_face(struct lws *wsi, struct face_recognition_payment_data_monitoring_agent *frc, json_object *jo_req) {


    PGresult    *res;
    char *stm;
    char *paramValues[4];

    std::string classifier;
    std::string modelface;
    std::string imageaddr;
    double confidence;



    // Get Haarcascade default model

    // Get address model face

    // Get image verified

    // Set Confidence Level

    // call function faceRecognition
    // retrun label by image receive
    faceRecognition(classifier, modelface, imageaddr, confidence);

}
