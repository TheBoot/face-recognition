/**
 * Filename: /home/raha/qere-socket/recognition-client.c
 * Path: /home/raha/qere-socket
 * Created Date: Monday, September 11th 2017, 2:51:22 pm
 * Author: raha
 * 
 * Copyright (c) 2017 PT Bahasa Kinerja Utama
 */


#include <time.h>
#include "recognition.h"
#include <json-c/json.h>
#include "recognition-client-function.h"

/** generic connection setting */
#define BUFFER_LEN              1024 * 1000

int seed, rc, n;
char p_len[4];

int callback_monitoring_agent(struct lws *wsi, enum lws_callback_reasons reason,
    void *user, void *in, size_t len){
        json_object *jo_req;
        json_object *obj_value;
        char cmd[256];
	int rc;

        // session data
        struct face_recognition_payment_data_monitoring_agent *frc  = 
            (struct face_recognition_payment_data_monitoring_agent *) user;
        
        switch (reason) {
            case LWS_CALLBACK_ESTABLISHED:
                lwsl_notice("callback established");
                // save on next usage, case on rhel wsi address/pointer was changed
                frc->wsi_ori = wsi;

                // prepare queue
                frc->q = queueCreate();

                rc = open_db_connection(frc);
		if (rc == -1) {
		    return -1;
		}
            break;
            case LWS_CALLBACK_SERVER_WRITEABLE:
                if (queueEmpty(frc->q) == 0) {
                    char out_brw[1024 * 1000];
                    int len;

                    if (deq(frc->q, &out_brw[0], &len) == 0) {
                        lws_write(wsi, (unsigned char *) out_brw, len, LWS_WRITE_TEXT);
                    }
                }
            break;
            case LWS_CALLBACK_RECEIVE:
                if (strncmp((char *)in,"{\"cmd\"", 6) == 0) {
                    n = 0;
                    
                    jo_req = json_tokener_parse((char *)in);
                    obj_value = json_object_object_get(jo_req, "cmd");
                    strcpy(cmd, json_object_get_string(obj_value));

                    lwsl_notice("in_brw : %s\n", in);

                    if (strncmp(cmd, "buyer-login", 11) == 0) {
                        n = login_response(wsi, frc, jo_req);
                    } else if (strncmp(cmd, "cashier-login", 13) == 0) {
                        n = login_response(wsi, frc, jo_req);
                    } else if (strncmp(cmd, "request-qr", 10) == 0) {
                        n = request_qr(wsi, frc, jo_req);
                    } else if (strncmp(cmd, "validate-qr", 11) == 0) {
                        n = validate_qr(wsi, frc, jo_req);
                    }/* else if (strncmp(cmd, "enroll-face", 11) == 0) {
                        n = enroll_face(wsi, frc, jo_req);
                    } else if (strncmp(cmd, "validate-face", 13) == 0) {
                        n = validate_face(wsi, frc, jo_req);
                    }*/
                    json_object_put(jo_req);
                }else {
                    write(frc->fd, in, len);
                }
            break;
            case LWS_CALLBACK_CLOSED:
                queueDestroy(frc->q);

                if(frc->fd > 2) {
                    close(frc->fd);
                }
            break;
            default:
            break;
        }
    return 0;
}
