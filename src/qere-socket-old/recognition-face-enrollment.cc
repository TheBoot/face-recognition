/*
 * =====================================================================================
 *
 *       Filename:  recognition-face-enrollment.c
 *
 *    Description:  Enroll Function Face Recognition
 *
 *        Version:  1.0
 *        Created:  13/09/17 10:06:54
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Izzul Haq Alhakam (al), izzul@bahasakita.co.id
 *   Organization:  PT. Bahasa Kinerja Utama
 *
 * =====================================================================================
 */

#include "recognition.h"
#include "recognition-client-function.h"
#include "FaceRecognize.hpp"


int enrollment_face(struct lws *wsi, struct face_recognition_payment_data_monitoring_agent *frc, json_object *jo_req) {

        PGresult    *res;
        char        *stm;
        char        *paramValues[4];
        const char  *pvalues1;
        char        *qrid;

        std::string      src_face;
        std::string      dest_face;
        std::string      cascadeName;
        std::string      nestedcascadeName;
        std::string      outcsvfile;
        std::string      filemodel;


        // Get Address Image Face Source Directory
        // Original Face From app


        // Get Address Destination Directory
        // Grayscale Face From Preparation


        // Get Address Cascade File (haarcascade_frontalface_alt.xml)


        // Get Address Nested Cascade (haarcascade_eye_tree_eyeglasses.xml)


        // Set Model Name
        // ex : "/home/alhakam/data_model/fisherface.yml"


        // Train Preparation
        // Save Grayscale face image to Destination Directory
        // if successed return 1
        trainPreparation(cascadeName, nestedcascadeName, src_face, dest_face, 2, true);

        // Create CSV
        // create file csv from dest_face
        // if successed return 1
        createCSV(dest_face, outcsvfile);

        // Train Data by fisherface
        // if successed return 1
        fisherfaceTrain(outcsvfile, filemodel);

        // get label from image train
        // and save to db by dest_face direcotry
        // return label if successed
        getlabel(outcsvfile, dest_face);

}
