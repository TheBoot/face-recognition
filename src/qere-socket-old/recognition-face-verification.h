/*
 * =====================================================================================
 *
 *       Filename:  recognition-face-verification.h
 *
 *    Description: 
 
 *        Version:  1.0
 *        Created:  13/09/17 16:11:10
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Izzul Haq Alhakam (al), izzul@bahasakita.co.id
 *   Organization:  PT. Bahasa Kinerja Utama
 *
 * =====================================================================================
 */

int validate_face(struct lws *wsi, struct face_recognition_payment_data_monitoring_agent *frc, json_object *jo_req);
