#include <iostream>
#include <string>

#include "bri.h"

int main(int argc, char const *argv[]) {
	if (argc != 6) {
		std::cout << "USAGE";
		std::cout << "\n./example.o <API KEY> <API SECRET>";
		std::cout << " <CLIENT ID> <CLIENT SECRET> <ACCOUNT NUMBER>";
		std::cout << "\nEXAMPLE\n";
		std::cout << "./example.o 9bf7b7a9dcecb730b0df8efd9f116f360dd184ed";
		std::cout << " 90c1d0d6340afbe4836d9f7829fe7a770f4e755d";
		std::cout << " ece249d7a7fcb7c51c7e7ad82761edd5616a";
		std::cout << " 3a8495aa862907ac161c2a960ecc28433cf1";
		std::cout << " 888801000105501";
		std::cout << std::endl;
		exit(1);
	}

	// General object
	pss obj;

	// Initialization
	bri_api::init(&obj, argv[1], argv[2], argv[3], argv[4]);

	// Print every response
	bri_api::set_print_response(&obj, true);

	// Set base url
	bri_api::set_base_url(&obj,
		"https://private-anon-8cd687e5dd-briapi.apiary-mock.com/v1/api/");

	// Set account number
	bri_api::set_account_number(&obj, argv[5]);

	// Request token
	bri_api::job(&obj, GET_TOKEN);

	// Request balance
	bri_api::job(&obj, GET_BALANCE);

	// Request statement
	bri_api::job(&obj, GET_STATEMENT, "{\"start_date\": \"2017-01-02\","
		"\"end_date\": \"2017-01-02\"}");

	// Request valas
	bri_api::job(&obj, GET_VALAS, "{\"dealt_currency\": \"USD\","
		"\"counter_currency\": \"IDR\"}");

	// Request location
	bri_api::job(&obj, GET_LOCATION, "{\"echannel\": \"atm\","
		"\"distance\": \"1\",\"latitude\":\"-6.30274\","
		"\"longitude\":\"106.82163\"}");

	// Post transfer
	bri_api::job(&obj, POST_TRANSFER, "{\"noreferral\": \"11112341512\","
		"\"noRekTujuan\": \"888801000001309\",\"amount\":\"100\"}");

	// Post transfer valas
	bri_api::job(&obj, POST_TRANS_VALAS, "{\"counter_currency\": \"IDR\","
		"\"counter_amount\": \"14000\",\"dealt_currency\":\"USD\","
		"\"dealt_amount\":\"1\",\"deal_rate\":\"14500\","
		"\"npwp\":\"747441809503000\", \"deal_type\":\"buy\"}");

	// Review status
	bri_api::job(&obj, STATUS);
	return 0;
}