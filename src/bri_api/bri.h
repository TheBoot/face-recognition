#ifndef _BRI_H_
#define _BRI_H_

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>

#include "json.hpp"
#include "define_struct_here.h"

#ifndef TASK_TYPE
#define TASK_TYPE
#define GET_TOKEN 0
#define GET_BALANCE 1
#define GET_STATEMENT 2
#define GET_VALAS 3
#define GET_LOCATION 4
#define POST_TRANSFER 50
#define POST_TRANS_VALAS 51
#define STATUS 99
#define ERROR_TASK -1
#endif  // TASK_TYPE

using json = nlohmann::json;

namespace bri_api {
	// Several helping function
	std::string execute_command(std::string cmd) {
		std::string data;
		FILE *stream;
		const int max_buffer = 256;
		char buffer[max_buffer];
		cmd.append(" 2>&1");

		stream = popen(cmd.c_str(), "r");
		if (stream) {
			while (!feof(stream))
			if (fgets(buffer, max_buffer, stream) != NULL) data.append(buffer);
			pclose(stream);
		}
		return data;
	}  // execute command

	std::string timestampISO8601(const int zone, const int offset) {
		std::string retval;
	    time_t timer;
	    char buffer[30];
	    struct tm* tm_info;

	    timer = time(NULL) + offset;
	    tm_info = localtime(&timer);

	    if (zone > 0 && zone < 10) {
	    	strftime(buffer, 26, "%Y-%m-%dT%H:%M:%S", tm_info);
	    	sprintf(buffer, "%s.000+0%d:00", buffer, zone);
		} else if (zone >= 10) {
			strftime(buffer, 26, "%Y-%m-%dT%H:%M:%S", tm_info);
	    	sprintf(buffer, "%s.000+%d:00", buffer, zone);
		} else if (zone < 0 && zone > -10) {
	    	strftime(buffer, 26, "%Y-%m-%dT%H:%M:%S", tm_info);
	    	sprintf(buffer, "%s.000-0%d:00", buffer, -zone);
		} else if (zone <= -10) {
			strftime(buffer, 26, "%Y-%m-%dT%H:%M:%S", tm_info);
	    	sprintf(buffer, "%s.000-%d:00", buffer, -zone);
		} else if (zone == 0) {
			strftime(buffer, 26, "%Y-%m-%dT%H:%M:%S", tm_info);
	    	sprintf(buffer, "%sZ", buffer);
		}

		retval = std::string(buffer);
		return retval;
	}  // timestampISO8601

	// End of helping function

	// Core function
	void init (pss *obj, const std::string api_key, const std::string api_secret,
			const std::string client_id, const std::string client_secret) {
		obj->api_key = api_key;
		obj->api_secret = api_secret;
		obj->client_id = client_id;
		obj->client_secret = client_secret;
		obj->print_response = 0;
		obj->timezone = 7;
		obj->warning_counter = 0;
		obj->balance = 0.0;
	}  // init

	void remove_inclusion() {
		if (execute_command("rm -- --include") == "") {
			// Curl inclusion is cleaned
		}
	}

	void set_print_response(pss *obj, const bool cond) {
		obj->print_response = cond;
	}  // set_print_response

	void set_timezone(pss *obj, const int zone) {
		obj->timezone = zone;
	}  // set_timezone

	void set_account_number(pss *obj, const std::string account_number) {
		obj->account_number = account_number;
	}  // set_account_number

	void set_base_url(pss *obj, const std::string base_url) {
		obj->base_url = base_url;
	}  // set_base_url

	int view_status(pss *obj, const std::string option) {
		std::stringstream ss;
		ss << "\n API KEY             : " << obj->api_key;
		ss << "\n API SECRET          : " << obj->api_secret;
		ss << "\n CLIENT ID           : " << obj->client_id;
		ss << "\n CLIENT SECRET       : " << obj->client_secret;
		ss << "\n ACCOUNT NUMBER      : " << obj->account_number;
		ss << "\n BALANCE             : " << obj->balance;
		ss << "\n USER NAME           : " << obj->user_name;
		ss << "\n USER NUMBER         : " << obj->user_number;
		ss << "\n USER STATUS         : " << obj->user_status;
		ss << "\n TOKEN               : " << obj->token;
		ss << "\n TOKEN CREATED       : " << obj->token_created_time;
		ss << "\n TOKEN EXPIRED       : " << obj->token_expired_time;
		ss << "\n LAST JOB            : " << obj->task_type
		   << " @ " << obj->timestamp;
		ss << "\n BASE URL            : " << obj->base_url;
		ss << "\n\n LAST RESPONSE =======================================\n";
		ss << obj->response;
		ss << "\n =====================================================\n";
		ss << "\n WARNING LOG =========================================\n";
		ss << " " << obj->warning_counter << " message(s)";
		ss << obj->warning;
		ss << "\n =====================================================\n";
		std::cout << ss.str() << std::endl;
		return 0;
	}  // view_status

	int get_token(pss *obj, const std::string option) {
		std::stringstream ss;
		json j;

		j["grant_type"] = "authorization_code";
		j["client_id"] = obj->client_id;
		j["client_secret"] = obj->client_secret;
		j["code"] = obj->api_secret;

		ss << "curl -s -D ";
		ss << "--include --request POST \\";
		ss << "--header \"X-BRI-KEY: " << obj->api_key << "\" ";
		ss << "--data-binary \"" << j.dump() << "\" ";
		ss << "'" << obj->base_url << "token'";

		obj->response = execute_command(ss.str());
		remove_inclusion();
		
		try {
			json jobj = json::parse(obj->response);
			int lifetime = jobj["expires_in"];

			obj->token = jobj["access_token"];
		    obj->token_created_time = timestampISO8601(obj->timezone, 0);
		    obj->token_expired_time = timestampISO8601(obj->timezone, lifetime);

			if (obj->print_response == 1) {
				ss.str("");
				ss << "\n\n RESPONSE =======================================\n";
				ss << " " << obj->task_type << " @ ";
				ss << timestampISO8601(obj->timezone, 0) << "\n";
				ss << obj->response;
				ss << "\n ================================================\n";
				std::cout << ss.str() << std::endl;
			}
		}

		catch(...) {
			obj->warning.append("\n ");
			obj->warning.append(timestampISO8601(obj->timezone, 0));
			obj->warning.append(" ERROR is occured");
			obj->warning_counter++;
			return 1;
		}

		return 0;
	}  // get_token

	int get_balance(pss *obj, const std::string option) {
		std::stringstream ss;

		ss << "curl -s -D ";
		ss << "--include ";
		ss << "--header \"Authorization: Bearer " << obj->token << "\" ";
		ss << "--header \"X-BRI-KEY: " << obj->api_key << "\" ";		
		ss << "'" << obj->base_url << "inquiry/" << obj->account_number << "'";

		// CAUTION : JSON is invalid from server, modification is needed
		obj->response = execute_command(ss.str());
		obj->response.erase(obj->response.size() - 1, 1);
		obj->response.erase(0, 1);
		obj->response.erase(0, 1);
		obj->response.append("}");
		remove_inclusion();
		
		try {
			json jobj = json::parse(obj->response);
			if (jobj["responseDescription"] == "Inquiry Success" &&
				jobj["status"] == true) {
				jobj = json::parse(jobj["data"].dump());
				obj->user_number = jobj["NoRekAsal"];
				obj->user_name = jobj["NamaRekAsal"];
				obj->user_status = jobj["StatusRekAsal"];
				obj->balance = jobj["SaldoRekAsal"];

				if (obj->print_response == 1) {
					ss.str("");
					ss << "\n\n ";
					ss << "RESPONSE =======================================\n";
					ss << " " << obj->task_type << " @ ";
					ss << timestampISO8601(obj->timezone, 0) << "\n";
					ss << obj->response;
					ss << "\n ";
					ss << "================================================\n";
					std::cout << ss.str() << std::endl;
				}
			} else {
			 	return 1;
			}
		}

		catch(...) {
			obj->warning.append("\n ");
			obj->warning.append(timestampISO8601(obj->timezone, 0));
			obj->warning.append(" ERROR is occured");
			obj->warning_counter++;
			return 1;
		}

		return 0;
	}  // get_balance

	int get_statement(pss *obj, const std::string option) {
		std::stringstream ss;
		json jobj = json::parse(option);
		std::string start_date;
		std::string end_date;

		try{
			start_date = jobj["start_date"];
			end_date = jobj["end_date"];
		}

		catch (...) {
			obj->warning.append("\n ");
			obj->warning.append(timestampISO8601(obj->timezone, 0));
			obj->warning.append(" ERROR is occured");
			obj->warning_counter++;
			return 1;	
		}

		ss << "curl -s -D ";
		ss << "--include ";
		ss << "--header \"Authorization: Bearer " << obj->token << "\" ";
		ss << "--header \"X-BRI-KEY: " << obj->api_key << "\" ";		
		ss << "'" << obj->base_url << "statement/" << obj->account_number;
		ss << "/" << start_date << "/" << end_date << "'";

		obj->response = execute_command(ss.str());
		remove_inclusion();
		
		try {
			jobj = json::parse(obj->response);
			obj->statement_log = obj->response;
			if (obj->print_response == 1) {
				ss.str("");
				ss << "\n\n ";
				ss << "RESPONSE =======================================\n";
				ss << " " << obj->task_type << " @ ";
				ss << timestampISO8601(obj->timezone, 0) << "\n";
				ss << obj->response;
				ss << "\n ";
				ss << "================================================\n";
				std::cout << ss.str() << std::endl;
			}				
		}

		catch(...) {
			obj->warning.append("\n ");
			obj->warning.append(timestampISO8601(obj->timezone, 0));
			obj->warning.append(" ERROR is occured");
			obj->warning_counter++;
			return 1;
		}

		return 0;
	}  // get_statement

	int get_valas(pss *obj, const std::string option) {
		std::stringstream ss;
		json jobj = json::parse(option);
		std::string dealt_currency;
		std::string counter_currency;

		try {
			dealt_currency = jobj["dealt_currency"];
			counter_currency = jobj["counter_currency"];
		}

		catch (...) {
			obj->warning.append("\n ");
			obj->warning.append(timestampISO8601(obj->timezone, 0));
			obj->warning.append(" ERROR is occured");
			obj->warning_counter++;
			return 1;	
		}

		ss << "curl -s -D ";
		ss << "--include ";
		ss << "--header \"Authorization: Bearer " << obj->token << "\" ";
		ss << "--header \"X-BRI-KEY: " << obj->api_key << "\" ";		
		ss << "'" << obj->base_url << "valas/getrate/";
		ss << dealt_currency << "/" << counter_currency << "'";

		obj->response = execute_command(ss.str());
		remove_inclusion();
		
		try {
			jobj = json::parse(obj->response);

			if (obj->print_response == 1) {
				ss.str("");
				ss << "\n\n ";
				ss << "RESPONSE =======================================\n";
				ss << " " << obj->task_type << " @ ";
				ss << timestampISO8601(obj->timezone, 0) << "\n";
				ss << obj->response;
				ss << "\n ";
				ss << "================================================\n";
				std::cout << ss.str() << std::endl;
			}
		}

		catch(...) {
			obj->warning.append("\n ");
			obj->warning.append(timestampISO8601(obj->timezone, 0));
			obj->warning.append(" ERROR is occured");
			obj->warning_counter++;
			return 1;
		}

		return 0;
	}  // get_valas

	int get_location(pss *obj, const std::string option) {
		std::stringstream ss;
		json jobj = json::parse(option);
		std::string echannel;
		std::string distance;
		std::string latitude;
		std::string longitude;

		try {
			echannel = jobj["echannel"];
			latitude = jobj["latitude"];
			distance = jobj["distance"];
			longitude = jobj["longitude"];
		}

		catch (...) {
			obj->warning.append("\n ");
			obj->warning.append(timestampISO8601(obj->timezone, 0));
			obj->warning.append(" ERROR is occured");
			obj->warning_counter++;
			return 1;	
		}

		ss << "curl -s -D ";
		ss << "--include ";
		ss << "--header \"Authorization: Bearer " << obj->token << "\" ";
		ss << "--header \"X-BRI-KEY: " << obj->api_key << "\" ";		
		ss << "'" << obj->base_url << "location/near/" << echannel << "/";
		ss << distance << "/" << latitude << "/" << longitude << "'";

		obj->response = execute_command(ss.str());
		remove_inclusion();
		
		try {
			jobj = json::parse(obj->response);

			if (obj->print_response == 1) {
				ss.str("");
				ss << "\n\n ";
				ss << "RESPONSE =======================================\n";
				ss << " " << obj->task_type << " @ ";
				ss << timestampISO8601(obj->timezone, 0) << "\n";
				ss << obj->response;
				ss << "\n ";
				ss << "================================================\n";
				std::cout << ss.str() << std::endl;
			}
		}

		catch(...) {
			obj->warning.append("\n ");
			obj->warning.append(timestampISO8601(obj->timezone, 0));
			obj->warning.append(" ERROR is occured");
			obj->warning_counter++;
			return 1;
		}

		return 0;
	}  // get_location

	int post_transfer(pss *obj, const std::string option) {
		std::stringstream ss;
		json jobj = json::parse(option);
		std::string noreferral;
		std::string noRekTujuan;		
		std::string amount;

		try {
			if(get_balance(obj, option) == 0) {
				// Get balance success
			}
		}

		catch (...) {
			obj->warning.append("\n ");
			obj->warning.append(timestampISO8601(obj->timezone, 0));
			obj->warning.append(" ERROR is occured");
			obj->warning_counter++;
			return 1;	
		}

		try {
			noreferral = jobj["noreferral"];
			noRekTujuan = jobj["noRekTujuan"];
			amount = jobj["amount"];
		}

		catch (...) {
			obj->warning.append("\n ");
			obj->warning.append(timestampISO8601(obj->timezone, 0));
			obj->warning.append(" ERROR is occured");
			obj->warning_counter++;
			return 1;	
		}

		ss << "curl -s -D ";
		ss << "--include ";
		ss << "--header \"Authorization: Bearer " << obj->token << "\" ";
		ss << "--header \"X-BRI-KEY: " << obj->api_key << "\" ";
		ss << "--data-binary \"{";
		ss << "\"noreferral\":\"" << noreferral << "\",";
		ss << "\"noRekAsal\":\"" << obj->account_number << "\",";
		ss << "\"noRekTujuan\":\"" << noRekTujuan << "\",";
		ss << "\"amount\":\"" << amount << "\",";
		ss << "\"remark\":\"" << "tes" << "\",";
		ss << "\"namaTrx\":\"" << "API Tes" << "\"";
		ss << "}\" ";
		ss << "'" << obj->base_url << "transfer" << "'";

		obj->response = execute_command(ss.str());
		remove_inclusion();
		
		try {
			jobj = json::parse(obj->response);

			if (obj->print_response == 1) {
				ss.str("");
				ss << "\n\n ";
				ss << "RESPONSE =======================================\n";
				ss << " " << obj->task_type << " @ ";
				ss << timestampISO8601(obj->timezone, 0) << "\n";
				ss << obj->response;
				ss << "\n ";
				ss << "================================================\n";
				std::cout << ss.str() << std::endl;
			}
		}

		catch(...) {
			obj->warning.append("\n ");
			obj->warning.append(timestampISO8601(obj->timezone, 0));
			obj->warning.append(" ERROR is occured");
			obj->warning_counter++;
			return 1;
		}

		return 0;
	}  // post_transfer	

	int post_trans_valas(pss *obj, const std::string option) {
		std::stringstream ss;
		json jobj = json::parse(option);
		std::string counter_currency;
		std::string counter_amount;		
		std::string dealt_currency;
		std::string dealt_amount;
		std::string deal_rate;
		std::string npwp;
		std::string deal_type;

		try {
			counter_currency = jobj["counter_currency"];
			counter_amount = jobj["counter_amount"];
			dealt_currency = jobj["dealt_currency"];
			dealt_amount = jobj["dealt_amount"];
			deal_rate = jobj["deal_rate"];
			npwp = jobj["npwp"];
			deal_type = jobj["deal_type"];
		}

		catch (...) {
			obj->warning.append("\n ");
			obj->warning.append(timestampISO8601(obj->timezone, 0));
			obj->warning.append(" ERROR is occured");
			obj->warning_counter++;
			return 1;	
		}

		ss << "curl -s -D ";
		ss << "--include ";
		ss << "--header \"Authorization: Bearer " << obj->token << "\" ";
		ss << "--header \"X-BRI-KEY: " << obj->api_key << "\" ";
		ss << "--data-binary \"{";
		ss << "\"counter_currency\":\"" << counter_currency << "\",";
		ss << "\"counter_amount\":\"" << counter_amount << "\",";
		ss << "\"dealt_currency\":\"" << dealt_currency << "\",";
		ss << "\"dealt_amount\":\"" << dealt_amount << "\",";
		ss << "\"deal_rate\":\"" << deal_rate << "\",";
		ss << "\"npwp\":\"" << npwp << "\",";
		ss << "\"deal_type\":\"" << deal_type << "\"";
		ss << "}\" ";
		ss << "'" << obj->base_url << "valas/insert" << "'";

		obj->response = execute_command(ss.str());
		remove_inclusion();
		
		try {
			jobj = json::parse(obj->response);

			if (obj->print_response == 1) {
				ss.str("");
				ss << "\n\n ";
				ss << "RESPONSE =======================================\n";
				ss << " " << obj->task_type << " @ ";
				ss << timestampISO8601(obj->timezone, 0) << "\n";
				ss << obj->response;
				ss << "\n ";
				ss << "================================================\n";
				std::cout << ss.str() << std::endl;
			}
		}

		catch(...) {
			obj->warning.append("\n ");
			obj->warning.append(timestampISO8601(obj->timezone, 0));
			obj->warning.append(" ERROR is occured");
			obj->warning_counter++;
			return 1;
		}

		return 0;
	}  // post_trans_valas	

	int job (pss *obj, const int task_type, const std::string option = "") {
		if (task_type == STATUS) {
			return view_status(obj, option);
		} else if (task_type == GET_TOKEN) {
			obj->task_type = GET_TOKEN;
			obj->timestamp = timestampISO8601(obj->timezone, 0);
			return get_token(obj, option);
		} else if (task_type == GET_BALANCE) {
			obj->task_type = GET_BALANCE;
			obj->timestamp = timestampISO8601(obj->timezone, 0);
			return get_balance(obj, option);
		} else if (task_type == GET_STATEMENT) {
			obj->task_type = GET_STATEMENT;
			obj->timestamp = timestampISO8601(obj->timezone, 0);
			return get_statement(obj, option);
		} else if (task_type == GET_VALAS) {
			obj->task_type = GET_VALAS;
			obj->timestamp = timestampISO8601(obj->timezone, 0);
			return get_valas(obj, option);
		} else if (task_type == GET_LOCATION) {
			obj->task_type = GET_LOCATION;
			obj->timestamp = timestampISO8601(obj->timezone, 0);
			return get_location(obj, option);
		} else if (task_type == POST_TRANSFER) {
			obj->task_type = POST_TRANSFER;
			obj->timestamp = timestampISO8601(obj->timezone, 0);
			return post_transfer(obj, option);
		} else if (task_type == POST_TRANS_VALAS) {
			obj->task_type = POST_TRANS_VALAS;
			obj->timestamp = timestampISO8601(obj->timezone, 0);
			return post_trans_valas(obj, option);
		} else {
			obj->task_type = ERROR_TASK;
			obj->timestamp = timestampISO8601(obj->timezone, 0);
			return 1;
		}
		return 1;
	}  // job
	// end of core function
}  // end of namespace
#endif  // _BRI_H_