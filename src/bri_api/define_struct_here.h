#ifndef _DEFINE_STRUCT_HERE_H_
#define _DEFINE_STRUCT_HERE_H_

// ============================================================= required struct
typedef struct pss pss;

struct pss {
	// Address
	std::string base_url;

	// Client variables
	std::string client_id;
	std::string client_secret;
	std::string api_key;
	std::string api_secret;

	// User account
	std::string account_number;

	// Transaction variables
	std::string response;
	std::string warning;
	std::string token;
	std::string timestamp;
	std::string token_created_time;
	std::string token_expired_time;
	std::string user_name;
	std::string user_number;
	std::string user_status;
	std::string balance;
	std::string statement_log;
	int timezone;
	int warning_counter;

	// Operation variables
	int task_type;
	bool print_response;
};

// =================================================== required struct ends here

#endif  // _DEFINE_STRUCT_HERE_H_