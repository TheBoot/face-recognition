/*
 * =====================================================================================
 *
 *       Filename:  facerec.cc
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  06/09/17 09:03:56
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Izzul Haq Alhakam (al), izzul@bahasakita.co.id
 *   Organization:  PT. Bahasa Kinerja Utama
 *
 * =====================================================================================
 */

#include <iostream>
#include <dirent.h>
#include <sys/stat.h>
#include <opencv2/opencv.hpp>


using namespace cv;
using std::string;
using std::vector;

void readFilenames(vector<string> &filenames, const string &directory) {

    DIR *dir;
    class dirent *ent;
    class stat st;

    dir = opendir(directory.c_str());
    while ((ent = readdir(dir)) != NULL) {
        const string file_name = ent->d_name;
        const string full_file_name = directory + "/" + file_name;

        if (file_name[0] == '.')
           continue;

        if (stat(full_file_name.c_str(), &st) == -1)
            continue;

        const bool is_directory = (st.st_mode & S_IFDIR) != 0;

        if (is_directory)
            continue;

        filenames.push_back(file_name);
        std::cout<<file_name<<std::endl;
    }
    closedir(dir);
}

void detect( Mat &img, CascadeClassifier& cascade,
             CascadeClassifier& nestedCascade,
             double scale, bool tryflip,const string &dest );

string cascadeName;
string nestedCascadeName;

int main(int argc, char** argv) {

    //string folder = argv[1];
    //string output = argv[2];
    vector<string> filenames;

    string folder;
    string dest;
    bool tryflip;
    CascadeClassifier cascade, nestedCascade;
    double scale;

    //char *imageName = argv[1];
    //char *cvtImageName = argv[2] ;


    /*if (argc < 4) {
        std::cout<<"Usage : facerec <source image> <out directory> "<<std::endl;
        return -1;
    }*/
    CommandLineParser parser(argc, argv,
            "{help h ||}"
            "{cascade|../../data/haarcascades/haarcascade_frontalface_alt.xml|}"
            "{nested-cascade|../../data/haarcascades/haarcascade_eye_tree_eyeglasses.xml|}"
            "{scale|1|}{try-flip||}{@directoryimage||}{@destination||}"
            );
    cascadeName = parser.get<string>("cascade");
    nestedCascadeName = parser.get<string>("nested-cascade");
    scale = parser.get<double>("scale");
    if (scale < 1)
        scale = 1;
    tryflip = parser.has("try-flip");
    folder = parser.get<string>("@directoryimage");
    dest = parser.get<string>("@destination");

    if (!parser.check()) {
        parser.printErrors();
        return 0;
    }
    if (!nestedCascade.load(nestedCascadeName))
        std::cerr << "WARNING: could not load classifier cascade for nested objects" << std::endl;
    if (!cascade.load(cascadeName)) {
        std::cerr << "Error: Could not load classifier cascade" << std::endl;
        return -1;
    }
    if (folder.empty()||dest.empty()) {
        std::cerr << "Error: Could not find image" << std::endl;
    }
    else {

        readFilenames(filenames, folder);

        std::cout<<folder<<std::endl;
        // Preparation Image Grayscale
        // Mat image;
        // image size 100x100 pixel
        // Size size(100,100);

        for (size_t i = 0; i < filenames.size(); ++i) {

        //image = imread( imageName, IMREAD_COLOR );

        Mat image = imread( folder + filenames[i], IMREAD_COLOR);

        if(!image.data) {
            std::cerr << "problem loading image!!" << std::endl;
        }

        Mat gray_image;
        Mat resize_image;

        std::cout<<folder+filenames[i]<<std::endl;

        detect(image, cascade, nestedCascade, scale, tryflip, dest+filenames[i]);

        /*
        cvtColor( image, gray_image, COLOR_BGR2GRAY);
        resize(gray_image, resize_image, size);

        imwrite(outdir+filenames[i], resize_image);

        namedWindow(folder+filenames[i], WINDOW_AUTOSIZE);
        namedWindow("Gray Image", WINDOW_AUTOSIZE);
        */

        }
    }
    waitKey(0);

    return 0;
}


void detect( Mat &img, CascadeClassifier& cascade,
             CascadeClassifier& nestedCascade,
             double scale, bool tryflip, const string &dest ) {

    double t = 0;
    vector<Rect> faces, faces2;

    const static Scalar colors[] = {
        Scalar(255,0,0),
        Scalar(255,128,0),
        Scalar(255,255,0),
        Scalar(0,255,255),
        Scalar(0,128,255),
        Scalar(0,255,255),
        Scalar(0,0,255),
        Scalar(255,0,255)
    };

    Mat gray, smallImg;

    cvtColor(img, gray, COLOR_BGR2GRAY);
    //Size size(100,100);
    double fx = 1 / scale;
    resize( gray, smallImg, Size(), fx, fx, INTER_LINEAR );
    //resize(gray, smallImg, size);

    t = (double)getTickCount();
    cascade.detectMultiScale(smallImg, faces, 1.1, 2, 0 | CASCADE_SCALE_IMAGE, Size(30,30));
    if ( tryflip ) {
        flip (smallImg, smallImg, 1);
        cascade.detectMultiScale(smallImg, faces2, 1.1, 2, 0 | CASCADE_SCALE_IMAGE, Size(30,30));
        for( vector<Rect>::const_iterator r = faces.begin(); r != faces2.end(); ++r) {
            faces.push_back(Rect(smallImg.cols - r->x -r->width,
                            r->y, r->width, r->height));
        }
    }
    t = (double)getTickCount() - t;
    printf("detection time = %g  ms\n", t*1000/getTickFrequency());

    for ( size_t i = 0; i < faces.size(); i++) {
        Rect r = faces[i];
        Mat smallImgROI;
        vector<Rect> nestedObjects;
        Point center;
        Scalar color = colors[i%8];
        int radius;

        double aspect_ratio = (double)r.width/r.height;

        if ( 0.75 < aspect_ratio && aspect_ratio < 1.3) {
            center.x = cvRound((r.x + r.width*0.5)*scale);
            center.y = cvRound((r.y + r.height*0.5)*scale);
            radius = cvRound((r.width + r.height)*0.25*scale);
            circle( img, center, radius, color, 3, 8, 0);
        }
        else {
            rectangle( img, cvPoint(cvRound(r.x*scale), cvRound(r.y*scale)),
                       cvPoint(cvRound((r.x + r.width-1)*scale), cvRound((r.y + r.height-1)*scale)),
                       color, 3, 8, 0);
        }
        if (nestedCascade.empty())
            continue;

        smallImgROI = smallImg( r );
        nestedCascade.detectMultiScale (smallImgROI, nestedObjects, 1.1, 2, 0 |CASCADE_SCALE_IMAGE,
                                         Size(30,30));
        
        // Save Image
        resize (smallImgROI, smallImgROI, Size(100,100));
        imwrite(dest, smallImgROI);
    }

}


