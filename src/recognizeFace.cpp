/*
 * =====================================================================================
 *
 *       Filename:  recognizeFace.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  08/09/17 16:08:05
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Izzul Haq Alhakam (al), izzul@bahasakita.co.id
 *   Organization:  PT. Bahasa Kinerja Utama
 *
 * =====================================================================================
 */

#include <opencv2/core.hpp>
#include <opencv2/face.hpp>
#include <opencv2/face/facerec.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/objdetect.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/imgproc.hpp>
#include <iostream>
#include <stdio.h>
#include <string>

using namespace cv;

using std::cout;
using std::cerr;
using std::endl;
using std::string;
using std::vector;
using std::ifstream;
using std::stringstream;

// save image and label
static void dbread( const string &filename, vector<Mat> &images, vector<int> &labels,
                    char separator = ';') {

    ifstream file(filename.c_str(), ifstream::in);

    if (!file) {
        string error =  "no valid input file";
        CV_Error(CV_StsBadArg, error);
    }

    string line, path, label;
    while (getline(file, line)) {
        stringstream liness(line);
        getline(liness, path, separator);
        getline(liness, label);
        if (!path.empty() && !label.empty()) {
            images.push_back(imread(path, 0));
            labels.push_back(atoi(label.c_str()));
        }
    }
}

string getlabel(const string &filename, const string &dir_image, char separator = ';') {

    ifstream file(filename.c_str(), ifstream::in);

    if (!file) {
        string error =  "no valid input file";
        CV_Error(CV_StsBadArg, error);
    }

    string line, path, label;
    while (getline(file, line)) {
        stringstream liness(line);
        getline(liness, path, separator);
        getline(liness, label);
        if (path.compare(dir_image)==0) {
            return label;
        }
    }
}

int main (int argc, char** argv) {

    // FisherFace Trainer
    vector<Mat> images;
    vector<int> labels;
    double confidence_level = std::stod(argv[2]);

    try {
        string csvfile = argv[1];
        dbread(csvfile, images, labels);
        string lablevalue = getlabel(csvfile, "/home/alhakam/Data/workspace/FaceRecognition/data_training/S31/2017-09-12-081637.jpg");

        cout << "label on image 02707 " << lablevalue << endl;

        cout << "size of the images is " << images.size() << endl;
        cout << "size of the labels is " << labels.size() << endl;
        cout << "Training begins ..... " << endl;
    }
    catch (cv::Exception &e) {
        cerr << "Error opening the file " << e.msg << endl;
        exit(1);
    }

    // create algorithm fisherface recognizer
    cv::Ptr<cv::face::FaceRecognizer> model = cv::face::FisherFaceRecognizer::create(0, confidence_level);
    //create algorithm fisherface recognizer
    //cv::Ptr<cv::face::FaceRecognizer> model = cv::face::EigenFaceRecognizer::create(0, confidence_level);
    model->train(images, labels);

    int height = images[0].rows;

    // Save Model fisherface
    model->write("/home/alhakam/Data/workspace/FaceRecognition/fisherface.yml");

    // Save model EigenFace
    //model->write("/home/alhakam/Data/workspace/FaceRecognition/eigenface.yml");

    cout << "Training Finished........" << endl;

    ////////////////////////////////////////////////////////////

    cout << "Start Recognizing..." << endl;
    // Begin face Recognizing with fisherface algorithm
    model->read("/home/alhakam/Data/workspace/FaceRecognition/fisherface.yml");

    // Begin face Recognizing with eigenface algorithm
    //model->read("/home/alhakam/Data/workspace/FaceRecognition/eigenface.yml");

    Mat testSample = imread("/home/alhakam/Data/workspace/FaceRecognition/data_training/S32/photo6174491589875967990.jpg", 0);

    int label_test = -1;
    double confidence_test = 0;
    model->predict(testSample, label_test, confidence_test);

    cout << " Threshold test : " << model->getThreshold() << endl;
    cout << " confidence test: " << confidence_test << endl;
    cout << " label test: " << label_test << endl;

    int img_width  = testSample.cols;
    int img_height = testSample.rows;

    // Load Clasifier Cascade
    // default haarcascade_frontalface_default.xml;
     string classifier = "/usr/local/share/OpenCV/haarcascades/haarcascade_frontalface_default.xml";
    // face trainnig
    // string classifier = "/home/alhakam/Data/workspace/FaceRecognition/classifier/cascade.xml";

    CascadeClassifier face_cascade;
    string window = "Capture - face detection";

    if (!face_cascade.load(classifier)) {
        cout << "Error loading file" << endl;
        return -1;
    }

    // Open Video Camera
    VideoCapture cap(0);

    if (!cap.isOpened()) {
        cout << "exit" << endl;
        return -1;
    }

    namedWindow(window, 1);
    long count = 0;

    while (true) {
        vector<Rect> faces;
        Mat frame;
        Mat grayScaleFrame;
        Mat original;
        
        cap >> frame;
        // Count Frames
        count = count + 1;

        if (!frame.empty()) {
            // clone from original frame
            original = frame.clone();

            // convert image to gray scale and equalize
            cvtColor(original, grayScaleFrame, CV_BGR2GRAY);

            // detect face in gray image
            face_cascade.detectMultiScale(grayScaleFrame, faces, 1.1, 5, 0, cv::Size(100,100));

            // number of faces detected
            cout << faces.size() << " faces detected" << endl;
            string frameset = std::to_string(count);
            string faceset = std::to_string(faces.size());

            int width  = 0;
            int height = 0;

            // Person Name
            string Pname = "";

            // Get Face
            for (int i = 0; i < faces.size(); i++) {

                    // region of interest
                    Rect face_i = faces[i];

                    // Crop the roi from gray image
                    Mat face = grayScaleFrame(face_i);

                    // Resizing the cropped image to
                    // suit database image sizes
                    Mat face_resized;
                    //resize(face, face_resized, Size(img_width, img_height), 1.0, 1.0, INTER_CUBIC);
                    resize(face, face_resized, Size(100, 100), 1.0, 1.0, INTER_CUBIC);

                    // Recognizing what faces detected
                    int label = -1;
                    double confidence = 0;

                    //cout << " face size : " << face_resized << endl;

                    model->predict(face_resized, label, confidence);

                    cout << " Threshold : " << model->getThreshold() << endl;
                    cout << " confidence : " << confidence << endl;
                    cout << " label : " << label << endl;

                    // Drawing Green rectangle in recognize face
                    rectangle(original, face_i, CV_RGB(0, 255, 0), 1);
                    string text = "Detected";

                    if  (label == 9 && confidence < 2700) {
                        Pname = "Izzul";
                    }
                    else if  (label == 7 && confidence < 6000) {
                        Pname = "Preste";
                    }
                    else if  (label == 11 && confidence < 5800) {
                        Pname = "Ica";
                    }
                    else {
                        Pname = "unknown";
                    }

                    int pos_x = std::max(face_i.tl().x - 10, 0);
                    int pos_y = std::max(face_i.tl().y - 10, 0);

                    // write name the person who is in the image
                    putText(original, Pname, Point(pos_x, pos_y), FONT_HERSHEY_COMPLEX_SMALL, 1.0, CV_RGB(0, 255, 0), 1.0);
            }

            putText(original, "Frames" + frameset, Point(30, 60), CV_FONT_HERSHEY_COMPLEX_SMALL, 1.0, CV_RGB(0, 255, 0), 1.0);
            putText(original, "Frames" + frameset, Point(30, 60), CV_FONT_HERSHEY_COMPLEX_SMALL, 1.0, CV_RGB(0, 255, 0), 1.0);

            imshow(window, original);
        }

        if (waitKey(30) >= 0)
            break;
    }
}
